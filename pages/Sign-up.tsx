import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import NavigationBar from "./components/NavigationBar"
import PageLoader from './components/PageLoader'
import Swal from 'sweetalert2'
import { registerAccount } from './helpers/authentication'

const SignIn = () => {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [userName, setUserName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const resetStates = () => {
      setFirstName('')
      setLastName('')
      setUserName('')
      setEmail('')
      setPassword('')
    }
  
    const submitForm = async (event:any) => { 
      event.preventDefault()
      if(firstName === '' || lastName === '' ||
         userName === '' || email === '' || password === ''){
           return (
            Swal.fire({
              title: 'Error!',
              text: 'Please fill up all fields',
              icon: 'error',
              confirmButtonText: 'Ok'
            })
           )
         }
      const userCredentials = {
        firstname: firstName,
        lastname: lastName,
        user_name: userName,
        email: email,
        password: password
      }
      const result = await registerAccount(userCredentials)
      console.log(result)
      if(result.status === 422) { 
        Swal.fire({
          title: 'Error!',
          text: result.data.error.messages.errors[0].message,
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }else{
        Swal.fire({
          title: 'Registration Success!',
          text: 'Account successfully registered',
          icon: 'success',
          confirmButtonText: 'Ok'
        }).then(() => {
          resetStates() 
        });
      }
    }

    return( 
        <>
        {<NavigationBar authenticated={false}/>}
    <div className="p-6 flex items-center justify-center">
    <div className="container max-w-screen-lg mx-auto">
      <div>
        <h2 style={{ fontSize: 30 }} className="font-semibold text-xl text-gray-600">Create an account</h2>
        <p className="mt-2 text-gray-500 mb-6 flex">Already have an account?
        
        <Link href="Sign-in">
        <a className="ml-2 font-bold" style={{
            color: '#55ad6d'
        }}>Login Here.</a>
       </Link>
        </p>
         <div className="bg-white rounded shadow-xl p-4 px-4 md:p-8 mb-6">
        <div className="grid gap-4 gap-y-2 text-sm grid-cols-1 lg:grid-cols-3">
          <div className="text-gray-600">
            <p className="font-medium text-lg">Personal Details</p>
            <p>Please fill out all the fields.</p>
          </div>

          <form onSubmit={submitForm} className="lg:col-span-2">
          <div>
            <div className="grid gap-4 gap-y-2 text-sm grid-cols-1 md:grid-cols-5">
              <div className="md:col-span-5">
                <label>First Name</label>
                <input
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                type="text" name="firstname" id="firstname" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50 focus:outline-none focus:ring-green-300 focus:border-green-300 focus:z-10" placeholder="John Michael"/>
              </div>
              <div className="md:col-span-5">
                <label>Last Name</label>
                <input 
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                type="text" name="lastname" id="lastname" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50 focus:outline-none focus:ring-green-300 focus:border-green-300 focus:z-10" placeholder="Walker"/>
              </div>
              <div className="md:col-span-5">
                <label>Email Address</label>
                <input 
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type="text" name="email" id="email" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50 focus:outline-none focus:ring-green-300 focus:border-green-300 focus:z-10" placeholder="email@domain.com" />
              </div>

              <div className="md:col-span-3">
                <label>Username</label>
                <input 
                value={userName}
                onChange={(e) => setUserName(e.target.value)}
                type="text" name="username" id="username" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50 focus:outline-none focus:ring-green-300 focus:border-green-300 focus:z-10" placeholder="Onceuponadime" />
              </div>

              <div className="md:col-span-2 mb-5">
                <label>Password</label>
                <input 
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type="password" name="password" id="password" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50 focus:outline-none focus:ring-green-300 focus:border-green-300 focus:z-10" placeholder="********" />
              </div>
              <div className="md:col-span-5 text-right">
                <div className="inline-flex items-end">
                  <button
                  type="submit"
                  className="bg-green-400 hover:bg-green-600 text-white font-bold py-2 px-4 rounded">Submit</button>
                </div>
              </div>

            </div>
          </div>
        </form>
        
        </div>
      </div>
      </div>
    </div>
  </div>
</>
    );
}

export default SignIn