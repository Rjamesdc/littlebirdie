import React, { useState, useEffect } from 'react'
import type { NextPage } from 'next'
import NavigationBar from './components/NavigationBar'
import PageLoader from './components/PageLoader'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { requestProducts } from './helpers/products'


const Sales: NextPage = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [products, setProducts] = useState([])

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 1000)
    getAllProducts()
  },[])

  const getAllProducts = async () => {
    const limit = 4
    const result = await requestProducts(limit)
    setProducts(result.data)
  }

  const Banners = [
    {
      id: 1,
      img: 'https://assets.littlebirdie.com.au/uploads/posts/image/6516/First_card.png'
    },
    {
      id: 2,
      img: 'https://assets.littlebirdie.com.au/uploads/posts/image/6518/Third_card.png'
    },
    {
      id: 3,
      img: 'https://assets.littlebirdie.com.au/uploads/posts/image/6517/Middle_card.png'
    }
  ]

  return (
    
    <>
    {
      isLoading ? <PageLoader /> 
      :  
      <React.Fragment>
      <NavigationBar authenticated={false}/>
      <main className="py-12 md:px-20 sm:px-14 px-6">
        <div className="mb-8">
            <img src="https://assets.littlebirdie.com.au/uploads/carousel/desktop_image/166/Xmas-Desktop-2.0.png"/>
        </div>

        <div className="bg-white">
      <div className="p-10">
        <div className="flex justify-between"> 
        <h2 className="text-2xl font-extrabold tracking-tight text-gray-900">{`${"Today's"} Top 50`}</h2>
        <h3 className="text-sm font-extrabold tracking-tight text-blue-400">See all products {<FontAwesomeIcon icon={faArrowRight} />}</h3>
        </div>
        <div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
          {products.map((product: any) => (
            <div 
            key={product.id} 
            className="group relative">
              <div className="w-full min-h-80 bg-gray-200 aspect-w-1 aspect-h-1 rounded-md overflow-hidden group-hover:opacity-75 lg:h-80 lg:aspect-none">
                <img
                  src={product.product_image}
                  alt={product.product_image}
                  className="w-full h-full object-center object-cover lg:w-full lg:h-full"
                />
              </div>
              <div className="mt-4 flex justify-between">
                <div>
                  <h3 className="text-sm text-gray-700">
                    <a href={product.product_link}>
                      <span aria-hidden="true" className="absolute inset-0" />
                      {product.product_name}
                    </a>
                  </h3>
                  <p className="mt-1 text-sm text-gray-500">{product.category}</p>
                </div>
                <p className="text-sm font-medium text-gray-900">{product.product_price}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
        
    <div className="flex justify-center px-5">
      {Banners.map((banner) => (
           <div key={banner.id} className="lg:card-side text-primary-content">
           <div className="px-2">
           <img style={{
               width: 500,
               height: 250
             }} src={banner.img}/>
           </div>
         </div> 
      ))}
    </div>
      </main>
      </React.Fragment>
    }
    </>
    )
}

export default Sales
