import React, { useState, useEffect } from 'react'
import type { NextPage } from 'next'
import NavigationBar from './components/NavigationBar'


const Categories: NextPage = () => {
  return (
    <NavigationBar authenticated={false}/>
    )
}

export default Categories
