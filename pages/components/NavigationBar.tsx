import React, {useState, useEffect} from "react";
import Link from 'next/link'
import { faBars, faSearch, faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { verifyUser } from "../helpers/verification";
import { useRouter } from "next/router";

const NavigationBar: NextPage = (props: any) => {
  const [navbarOpen, setNavbarOpen] = useState(false);
  const [isVisible, setVisible] = useState(false)
  const [avatarVisible, setAvatarVisible] = useState(false)
  const [isAuth, setIsAuth] = useState(false)
  const router = useRouter()

  useEffect(() => {
    isAuthenticated()
  }, [])

  const isAuthenticated = async () => {
      const res = await verifyUser()
      if(!res) {
        setIsAuth(false)
       }else{
        if(router.pathname === '/Sign-in' || router.pathname === '/Sign-up'){
          router.push('/')
          setIsAuth(true)
        }
         setIsAuth(true)
       }
  }

  const logout = async () => {
    localStorage.clear()
    router.push("/Sign-in")
  }

  const handleClick = (e) =>  {
    if(navbarOpen) return setNavbarOpen(false)
    setNavbarOpen(true)
    }
    
  return (
    <>
    <nav style={{ backgroundColor:'#DCFAD2' }} className="sticky top-0 z-50 shadow dark:bg-gray-800">
        <div className="container px-6 py-4 mx-auto lg:flex lg:justify-between lg:items-center">
            <div className="lg:flex lg:items-center">
                <div className="flex items-center justify-between">
                    <div>
                      <img src='https://www.littlebirdie.com.au/packs/media/branding/littlebirdie-logo-nav-ada8b5f3.svg'/>
                    </div>

                    <div className="flex lg:hidden">
                        <button 
                        onClick={handleClick}
                        type="button" className="text-gray-500 dark:text-gray-200 hover:text-gray-600 dark:hover:text-gray-400 focus:outline-none focus:text-gray-600 dark:focus:text-gray-400" aria-label="toggle menu">
                           <FontAwesomeIcon icon={faBars}/>
                        </button>
                    </div>
                </div>

                <div className={`flex flex-col text-gray-600 capitalize dark:text-gray-300 lg:flex lg:px-16 lg:-mx-4 lg:flex-row lg:items-center ${navbarOpen ? '' : 'hidden'}`}>
                <div className="relative mt-4 lg:mt-0 lg:mx-4">
                        <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                           <FontAwesomeIcon icon={faSearch}/>
                        </span>

                        <input type="text" className="w-full py-1 pl-10 pr-4 text-gray-700 placeholder-gray-600 bg-white border-b border-gray-600 dark:placeholder-gray-300 dark:focus:border-gray-300 lg:w-100 lg:border-transparent dark:bg-gray-800 dark:text-gray-300 focus:outline-none focus:border-gray-600" placeholder="Search" />
                    </div>
                    <div className="relative group">
                            <a 
                            onClick={() => { isVisible ? setVisible(false) : setVisible(true) }}
                            href="#" className="mt-2 lg:mt-0 lg:mx-5 lg:flex hover:text-gray-800 dark:hover:text-gray-200 group-hover:border-grey-light">
                                Categories{" "}
                                <FontAwesomeIcon className="mt-1 ml-2" icon={faCaretDown}/>
                            </a>

                            <div style={{ width: 200 }} className={`items-center absolute border border-t-0 rounded-b-lg p-1 ${isVisible ? 'visible':'invisible'} group-hover:visible bg-white p-2`}>
                               <a 
                               href="#" className="px-4 py-2 block text-black hover:bg-grey-lighter">Manage Stores</a>
                               <a href="#" className="px-4 py-2 block text-black hover:bg-grey-lighter">Manage Users</a>
                               <a href="#" className="px-4 py-2 block text-black hover:bg-grey-lighter">Example Nav</a>
                            </div>
                        </div>
                    <Link href="/Categories">
                    <a href="#" className="mt-2 lg:mt-0 lg:mx-4 hover:text-gray-800 dark:hover:text-gray-200">Price Drops</a>
                    </Link>
                    <Link href="/Sales">
                    <a href="#" className="mt-2 lg:mt-0 lg:mx-4 hover:text-gray-800 dark:hover:text-gray-200">Sales</a>
                    </Link>
                    <Link href="/Vouchers">
                    <a href="#" className="mt-2 lg:mt-0 lg:mx-4 hover:text-gray-800 dark:hover:text-gray-200">Vouchers</a>
                    </Link>
                    {!isAuth &&
                     <Link href="Sign-in">
                     <a className="mt-2 lg:mt-0 lg:mx-4 hover:text-gray-800 dark:hover:text-gray-200">Join/Login</a>
                     </Link>}
                </div>
            </div>

            {isAuth &&
               <div className={`flex mt-6 lg:flex lg:mt-0 lg:-mx-2 ${navbarOpen ? '' : 'hidden'}`}>

            <div className="relative group">
                            <a onClick={() => { avatarVisible ? setAvatarVisible(false) : setAvatarVisible(true) }}>
                              <img style={{
                                width: 40,
                                height: 40,
                                borderRadius: 50
                              }} src='https://www.w3schools.com/howto/img_avatar.png'/>
                          </a>

                            <div style={{ width: 200 }} className={`items-center absolute border border-t-0 rounded-b-lg p-1 ${avatarVisible ? 'visible':'invisible'} group-hover:visible bg-white p-2`}>
                               <Link href={router.pathname === '/user/Profile' ? router.pathname : 'user/Profile'}>
                               <a href="#" className="px-4 py-2 block text-black hover:bg-grey-lighter">Profile</a>
                               </Link>
                               {/* <a href="#" className="px-4 py-2 block text-black hover:bg-grey-lighter">Manage Users</a> */}
                               <button onClick={logout} className="px-4 py-2 block text-red-400 hover:bg-grey-lighter">Logout</button>
                            </div>
                        </div>
           </div>}
            
        </div>
    </nav>
    </>
  );
}

export default NavigationBar