import React from "react";
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const PageLoader = () => {
    return (
        <>
            <link rel="stylesheet" href="https://pagecdn.io/lib/font-awesome/5.10.0-11/css/all.min.css" integrity="sha256-p9TTWD+813MlLaxMXMbTA7wN/ArzGyW/L7c5+KkjOkM=" />

            <div className="w-full h-full fixed block top-0 left-0 bg-white opacity-100 z-50">
                <span className="text-green-500 opacity-100 top-1/2 my-0 mx-auto block relative w-0 h-0" style={{
                    top: '50%'
                }}>
                    <FontAwesomeIcon className="fa-spin" size="5x" icon={faCircleNotch}/>
                </span>
            </div>
        </>
    )
}

export default PageLoader