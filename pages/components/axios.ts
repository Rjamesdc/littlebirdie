import axios from "axios";
// import camelcaseKeys from "camelcase-keys";
// import snakecaseKeys from "snakecase-keys";
// import { parseCookies } from "nookies";
import Router from "next/router";

const snakeToCamel = (str) =>
	str.replace(/([-_]\w)/g, (g) => g[1].toUpperCase());
const camelToSnake = (str) =>
	str
		.replace(/[\w]([A-Z])/g, function (m) {
			return `${m[0]}_${m[1]}`;
		})
		.toLowerCase();

// https://github.com/axios/axios#request-config
const customAxios = axios.create({
	baseURL: process.env.NEXT_PUBLIC_CLONEABLE_USER_URL,
	withCredentials: true,
});

customAxios.interceptors.request.use(
	function (config) {
		const userType = Router.router.pathname.split("/")[1];
		const { transformToCamelCase = true } = config;
		const isFormData = config.data instanceof FormData;

		const configCopy = Object.assign({}, config);
		let tokens = [
			{
				type: "buyer",
				token: parseCookies("token").buyerToken,
			},
			{
				type: "seller",
				token: parseCookies("token").sellerToken,
			}
		];
		const token = tokens.find((token) => token.type === userType).token;

		if (token) {
			configCopy.headers["Authorization"] = "Bearer " + token;
		}
		if (configCopy.data && transformToCamelCase) {
			if (isFormData) {
				const formData = new FormData();

				for (const pair of configCopy.data.entries()) {
					formData.set(camelToSnake(pair[0]), pair[1]);
				}

				configCopy.data = formData;
			} else {
				configCopy.data = snakecaseKeys(configCopy.data, { deep: true });
			}
		}

		return configCopy;
	},
	function (error) {
		// Do something with request error
		return Promise.reject(error);
	}
);

customAxios.interceptors.response.use(
	function (response) {
		const { transformToCamelCase = true } = response.config;

		return {
			...response,
			data:
				response.data && transformToCamelCase
					? camelcaseKeys(response.data, { deep: true })
					: response.data,
		};
	},
	function (error) {
		const { transformToCamelCase = true } = error.response.config;

		if (error.response.status === 422 && transformToCamelCase) {
			if (Array.isArray(error.response.data.errors)) {
				error.response.data.errors = error.response.data.errors.map(
					(error) => ({
						...error,
						field: snakeToCamel(error.field),
						message:
							error.message.charAt(0).toUpperCase() +
							error.message.slice(1).replace(/_/g, " "),
					})
				);
				error.response.data.errors.map((error) => {
					console.log(
						error.message.charAt(0).toUpperCase() +
							error.message.slice(1).replace(/_/g, " ")
					);
				});
			}
		}

		return Promise.reject(error);
	}
);

export default customAxios;