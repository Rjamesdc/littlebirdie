import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import NavigationBar from "./components/NavigationBar"
import { login } from './helpers/authentication'
import Swal from 'sweetalert2'
import { useRouter, withRouter } from 'next/router'


const SignIn = (props: any) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const router = useRouter()

    useEffect(() => {
      authenticationNotif()
    },[])

    const authenticationNotif = async () => {
      const param = props.router.query.from
      if(param){
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        
        Toast.fire({
          icon: 'error',
          title: 'You must be logged in to access this page.'
        })
      }
    }

    const resetStates = () => {
      setEmail('')
      setPassword('')
    }

    const onLogin = async (event: any) => {
      event.preventDefault()
      const userCredentials = {
        email,
        password
      }
      const result = await login(userCredentials)
      if(result.status === 401) { 
        Swal.fire({
          title: 'Error!',
          text: result.data.message,
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }else{
        localStorage.setItem('session', JSON.stringify(result.data.session))
        localStorage.setItem('token', result.data.token.token)
        localStorage.setItem('isAuth', 'true')
        router.push('/')
        resetStates()
      }
    }

    return( 
        <>
        {<NavigationBar authenticated={false}/>}
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
  <div className="max-w-md w-full space-y-8">
    <div>
    <img className="mx-auto h-12 w-auto" src="https://www.littlebirdie.com.au/packs/media/branding/littlebirdie-logo-nav-ada8b5f3.svg"/>
      <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
        Sign in to your account
      </h2>
      <p className="mt-2 text-center text-sm text-gray-600">
        Or{" "}
        <Link href="Sign-up">
        <a className="font-bold" style={{
            color: '#55ad6d'
        }}>Create a free account</a>
      </Link>
      </p>
    </div>

    
    <form className="mt-8 space-y-6" onSubmit={onLogin}>
      <input type="hidden" name="remember" value="true"/>
      <div className="rounded-md shadow-sm -space-y-px">
        <div>
          <label className="sr-only">Email address</label>
          <input 
          onChange={(e) => setEmail(e.target.value)}
          value={email}
          id="email-address" name="email" type="email" className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-green-300 focus:border-green-300 focus:z-10 sm:text-sm" placeholder="Email address"/>
        </div>
        <div>
          <label className="sr-only">Password</label>
          <input 
          onChange={(e) => setPassword(e.target.value)}
          value={password}
          id="password" name="password" type="password"  className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-green-300 focus:border-green-300 focus:z-10 sm:text-sm" placeholder="Password"/>
        </div>
      </div>

      <div className="flex items-center justify-between">
        <div className="text-sm">
          <a href="#" className="font-extrabold" style={{
              color: '#55ad6d'
          }}>
            Forgot your password?
          </a>
        </div>
      </div>

      <div>
        <button 
        style={{
            backgroundColor: '#55ad6d'
        }}
        type="submit" className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white">
          Sign in
        </button>
      </div>
    </form>


  </div>
</div>
</>
    );
}

export default withRouter(SignIn)