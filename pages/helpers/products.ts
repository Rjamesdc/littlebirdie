import API from './apiBackend'

// function for register account
export const requestProducts = async (limit: any) => {

    const result = await API.get(`api/products/${limit}`)
        .then((response) => {
            return response;
        })
        .catch((error) => {
            console.log('Get Products Err', error)
            return error.response
        });

    return result;
}