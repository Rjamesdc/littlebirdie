import axios from "axios";
import API from './apiBackend'

// function for register account
export const registerAccount = async (state: any) => {

    const result = await API.post(`api/register`, state)
        .then((response) => {
            return response;
        })
        .catch((error) => {
            return error.response
        });

    return result;
}

//function for login account
export const login = async (state: any ) => {
    const result = await API.post(`api/login`, state)
    .then((response) => {
        return response;
    })
    .catch((error) => {
        return error.response
    })

    return result
}

//function for token authentication 
export const me = async (id: any, params: any) => {
    const result = await API.get(`api/verify/${id}`, 
    { 
        headers: {
          Authorization: `${params.token_type} ${params.access_token}`,
          Accept: 'application/json',
          'Accept-Language': 'en',
        }
    }).then((response) => {
        return response
    }).catch((error) => {
        return error.response
    })

    return result
}