import { me } from "./authentication"

export const verifyUser = async () => {
    const session: any = await localStorage.getItem('session')
    const token = await localStorage.getItem('token')
    if(session && token) {
    const userData = JSON.parse(session)
    const headers = {
      token_type: 'bearer',
      access_token: token
    }
    const result = await me(userData.id, headers)
    if(result.status !== 200){
        localStorage.clear()
        return false
    }
    return true
  }
    return false  
}